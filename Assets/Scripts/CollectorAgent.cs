using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Random = UnityEngine.Random;

public class CollectorAgent : Agent
{
    Rigidbody rBody;
    public float moveSpeed = 2f;
    public float rotationDegree = 1;
    public int korb = 0;
    public int korbMax = 10;
    public int collectedApple = 0;
    public int collectedBadApple = 0;
    public int collectedRottenApple = 0;
    public int moveToGoal = 3;
    public Transform Target;
    public AppleCollectorArea appleCollectorArea;
    public GameObject AppleCreator;
    public GameObject VisualApple;
    public GameObject VisualBadApple;
    public GameObject VisualRottenApple;
    public Material AgentMaterial;
    public Material FullKorb;
    public float respawnTime = 120;
    public float respawnTimer;
    public int seconds;
    public float distanceToTarget;
    public bool naeher;
    public bool grounded;



    void Start()
    {
        rBody = GetComponent<Rigidbody>();
        distanceToTarget = Vector3.Distance(this.transform.localPosition, Target.transform.position);
    }

    void FixedUpdate()
    {
        respawnTimer -= Time.deltaTime;

        if (respawnTimer <= 0)
        {
            AddReward(-100f);
            EndEpisode();
        }


        fixFallThrough();

    }

    public void fixFallThrough()
    {
        // prevent glitch through ground
        if (rBody.position.y < appleCollectorArea.transform.position.y + 0.49f)
        {
            rBody.angularVelocity = Vector3.zero;
            rBody.velocity = Vector3.zero;
            transform.localPosition = new Vector3(rBody.position.x, appleCollectorArea.transform.position.y + 0.5f, rBody.position.z);
        }
    }


    //public GameObject Target;

    public override void OnEpisodeBegin()
    {
        korb = 0;
        collectedApple = 0;
        collectedBadApple = 0;
        collectedRottenApple = 0;
        respawnTimer = respawnTime;
        moveToGoal = 3;
        ResetAgent();
        setAgentStatus();

        appleCollectorArea.CreateFruitTrees();

        GameObject[] rottenApples = GameObject.FindGameObjectsWithTag("RottenApple");
        foreach (GameObject ra in rottenApples)
        {
            Destroy(ra);
        }
        GameObject[] apples = GameObject.FindGameObjectsWithTag("Apple");
        foreach (GameObject a in apples)
        {
            Destroy(a);
        }
        GameObject[] badApples = GameObject.FindGameObjectsWithTag("BadApple");
        foreach (GameObject ba in badApples)
        {
            Destroy(ba);
        }

        // If the Agent fell, zero its momentum
        //if (this.transform.localPosition.y < 0)
        //{

        //}

    }

    public void ResetAgent()
    {
        float range = 6f;
        while (true)
        {
            float initx = Random.Range(-range, range);
            float initz = Random.Range(-range, range);

            Vector3 potentialPos = new Vector3(initx, 2.1f, initz);

            Collider[] colliders = Physics.OverlapSphere(potentialPos, 2f);
            bool safePosFound = colliders.Length == 0;

            if (safePosFound)
            {
                this.rBody.angularVelocity = Vector3.zero;
                this.rBody.velocity = Vector3.zero;
                this.transform.localPosition = new Vector3(Random.Range(-5f, 5f), 0.5f, Random.Range(-5f, 5f));
            }

            if (safePosFound)
            {
                break;
            }
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Target.transform.position);
        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
        sensor.AddObservation(rBody.velocity.y);

        sensor.AddObservation(korb);
    }



    public void MoveAgent(float[] vectorAction)
    {
        //Ray ray = new Ray(transform.position, -transform.up);
        //Physics.Raycast(ray, out RaycastHit hit);

        grounded = Physics.Raycast(transform.position, Vector3.down, 1f);

        //Vector3 vectorDiff = hit.normal - transform.up;
        //if (hit.normal != Vector3.zero)
        //{
        //    transform.up = hit.normal;
        //}
        //transform.Rotate(vectorDiff);

        var dirToGo = Vector3.zero;

        var forward = Mathf.Clamp(vectorAction[1], -1f, 1f);
        var right = Mathf.Clamp(vectorAction[0], -1f, 1f);

        dirToGo = transform.forward * forward;
        dirToGo += transform.right * right;
        transform.Rotate(new Vector3(0, vectorAction[2] * rotationDegree, 0));

        rBody.AddForce(dirToGo, ForceMode.VelocityChange);

        if (rBody.velocity.sqrMagnitude > 25f) // slow it down
        {
            rBody.velocity *= 0.95f;
        }
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        float distanceToTargetOld = distanceToTarget;
        MoveAgent(vectorAction);
        AddReward(-0.01f);
        distanceToTarget = Vector3.Distance(this.transform.localPosition, Target.transform.position);



        if (korb >= korbMax)
        {
            if (distanceToTarget < distanceToTargetOld)
            {
                AddReward(0.001f);
            }
            else
            {
                AddReward(-0.01f);
            }
        }
        //else if (distanceToTargetNew != distanceToTargetOld){
        //    AddReward(-0.1f);
        //}



        // Fell off platform
        if (grounded == false)
        {
            AddReward(-300f);
            EndEpisode();
        }

    }


    void OnCollisionEnter(Collision collision)
    {
        korb = collectedApple + collectedBadApple + collectedRottenApple;


        if (collision.gameObject.CompareTag("Apple") && (korb < korbMax))
        {
            collision.gameObject.GetComponent<FoodLogic>().OnCollect();
            collectedApple += 1;
            print("collectedApple: " + collectedApple);
            AddReward(10f);
            setAgentStatus();
        }
        if (collision.gameObject.CompareTag("BadApple") && (korb < korbMax))
        {
            collision.gameObject.GetComponent<FoodLogic>().OnCollect();
            collectedBadApple += 1;
            print("collectedBadApple: " + collectedBadApple);
            AddReward(2f);
            setAgentStatus();
        }
        if (collision.gameObject.CompareTag("RottenApple") && (korb < korbMax))
        {
            collision.gameObject.GetComponent<FoodLogic>().OnCollect();
            collectedRottenApple += 1;
            print("collectedRottenApple: " + collectedRottenApple);
            AddReward(-1f);
            setAgentStatus();
        }

        if (korb >= korbMax)
        {
            print("korb ist voll");
        }

        //reward + apples f?r anstupsen der kiste 
        if (collision.gameObject.CompareTag("Goal"))
        {
            rBody.angularVelocity = Vector3.zero;
            rBody.velocity = Vector3.zero;
            transform.localPosition = new Vector3(rBody.position.x, appleCollectorArea.transform.position.y + 0.5f, rBody.position.z);

            float ablagereward = 0f;
            ablagereward = 1f * collectedApple + 0.5f * collectedBadApple + 0.01f * collectedRottenApple;
            ablagereward = 0.2f*Mathf.Exp((float)korb) + ablagereward + 1f;

            //instanciate visualapples according to what the agent has collected
            float X = AppleCreator.transform.position.x;
            float currentY = AppleCreator.transform.position.y;
            float Z = AppleCreator.transform.position.z;

            for (int i = 0; i < collectedApple; i++)
            {
                float currentX = Random.Range(X - 1f, X + 1f);
                float currentZ = Random.Range(Z - 1f, Z + 1f);
                Instantiate(VisualApple, new Vector3(currentX, currentY, currentZ), Quaternion.identity);
            }

            for (int i = 0; i < collectedBadApple; i++)
            {
                float currentX = Random.Range(X - 1f, X + 1f);
                float currentZ = Random.Range(Z - 1f, Z + 1f);
                Instantiate(VisualBadApple, new Vector3(currentX, currentY, currentZ), Quaternion.identity);
            }

            for (int i = 0; i < collectedRottenApple; i++)
            {
                float currentX = Random.Range(X - 1f, X + 1f);
                float currentZ = Random.Range(Z - 1f, Z + 1f);
                Instantiate(VisualRottenApple, new Vector3(currentX, currentY, currentZ), Quaternion.identity);
            }

            collectedApple = 0;
            collectedBadApple = 0;
            collectedRottenApple = 0;
            print("korb geleert: " + collectedApple + " " + collectedBadApple + " " + collectedRottenApple);
            AddReward(ablagereward);


            moveToGoal--;
            if (moveToGoal == 0)
            {
                EndEpisode();
            }


        }

        if (collision.gameObject.CompareTag("Rock"))
        {
            AddReward(-5f);
        }
        
    }


    public override void Heuristic(float[] vectorAction)
    {
        vectorAction[0] = Input.GetAxis("Horizontal");
        vectorAction[1] = Input.GetAxis("Vertical");
        float qe = 0;
        if (Input.GetKey(KeyCode.Q))
        {
            qe = -1;
        }
        else if (Input.GetKey(KeyCode.E))
        {
            qe = 1;
        }
        vectorAction[2] = qe;
    }

    public void createApples(GameObject appleCreator)
    {
        //instanciate visualapples according to what the agent has collected

        appleCreator = AppleCreator;

        float currentX = gameObject.transform.position.x;
        float currentY = gameObject.transform.position.y;
        float currentZ = gameObject.transform.position.z;

        for (int i = 0; i <= collectedApple; i++)
        {
            Instantiate(VisualApple, new Vector3(currentX, currentY, currentZ), Quaternion.identity);
        }

        for (int i = 0; i <= collectedBadApple; i++)
        {
            Instantiate(VisualBadApple, new Vector3(currentX, currentY, currentZ), Quaternion.identity);
        }

        for (int i = 0; i <= collectedRottenApple; i++)
        {
            Instantiate(VisualRottenApple, new Vector3(currentX, currentY, currentZ), Quaternion.identity);
        }


    }

    public void setAgentStatus()
    {
        korb = collectedApple + collectedBadApple + collectedRottenApple;
        if (korb >= korbMax)
        {
            Renderer rend = gameObject.GetComponent<Renderer>();
            rend.material = FullKorb;
        }
        if (korb < korbMax)
        {
            Renderer rend = gameObject.GetComponent<Renderer>();
            rend.material = AgentMaterial;
        }
    }



}