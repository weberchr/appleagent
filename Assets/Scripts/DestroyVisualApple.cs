﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyVisualApple : MonoBehaviour
{

    public GameObject VisualApple;
    public GameObject VisualBadApple;
    public GameObject VisualRottenApple;
    public float timer = 900;


    void Start()
    {
  
    }

    void Update()
    {
        timer -= Time.deltaTime;
        int seconds = (int)timer % 60;
        if (seconds <= 0)
        {

            Destroy(gameObject);
        }

    }

}