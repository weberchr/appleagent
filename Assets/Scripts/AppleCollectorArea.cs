﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AppleCollectorArea : MonoBehaviour
{
    public int numTrees;
    public int grassDensity = 10;
    public int range;
    public int numGoals;
    public GameObject FruitTree;
    public GameObject GrassType1;
    public GameObject GrassType2;
    public GameObject Goal;
    public List<GameObject> FruitTreeList = new List<GameObject>();
    public List<GameObject> GrassList = new List<GameObject>();


    void Start()
    {

        //CreateFoodObjects(numGoals, Goal);

    }

    public void CreateFruitTrees()
    {

        foreach (GameObject ft in FruitTreeList)
        {
            Destroy(ft);
        }
        FruitTreeList.Clear();
        FruitTreeList = CreateFoodObjects(numTrees, FruitTree);
    }


    List<GameObject> CreateFoodObjects(int num, GameObject type)
    {
        //float x = Goal.transform.localPosition.x;
        //float z = Goal.transform.localPosition.z;

        List<GameObject> instObjects = new List<GameObject>();
        List<GameObject> instGrassObjects = new List<GameObject>();


        for (int i = 0; i < num; i++)
        {
            int maxCounter = 0;
            while (true)
            {
                float initx = Random.Range(-range, range);
                float initz = Random.Range(-range, range);

                Vector3 potentialPos = new Vector3(initx, 3.1f, initz);

                Collider[] colliders = Physics.OverlapSphere(potentialPos, 3f);
                bool safePosFound = colliders.Length == 0;

                if (safePosFound)
                {
                    GameObject f = Instantiate(type, new Vector3(initx, 0f, initz) + transform.position, Quaternion.identity);
                    instObjects.Add(f);
                    for (int j = 0; j < grassDensity; j++)
                    {
                        float grassX = Random.Range(initx - 2f, initx + 2f);
                        float grassZ = Random.Range(initz - 2f, initz + 2f);

                        if (j % 2 == 0)
                        {
                            Instantiate(GrassType1, new Vector3(grassX, 0f, grassZ), Quaternion.identity);
                        }
                        else
                        {
                            Instantiate(GrassType2, new Vector3(grassX, 0f, grassZ), Quaternion.identity);
                        }

                    }

                }

                if (safePosFound || maxCounter > 20)
                {
                    maxCounter = 0;
                    break;
                }
            }
        }

        return instObjects;
    }

}