﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadAppleTimer : MonoBehaviour
{
    public bool timerActive = false;
    public float timerOfBadApple = 10;
    private Componsate compoScript;


    // Start is called before the first frame update
    void Start()
    { 
        timerActive = true;
        //goes through the scence and searches for an Object from the Script
        compoScript = FindObjectOfType<Componsate>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timerActive == true)
        {

            timerOfBadApple -= Time.deltaTime;
            int seconds = (int)timerOfBadApple % 60;
//            print(seconds);

            if (seconds <= 0)
            {
                timerActive = false;
                compoScript.compostate(gameObject);

            }
        }
    }
}