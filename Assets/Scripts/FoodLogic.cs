using UnityEngine;

public class FoodLogic : MonoBehaviour
{
    public bool respawn;
    public AppleCollectorArea myArea;
    public GameObject BadApple;
    public GameObject applePrefab;

    public void OnCollect()
    {
        Destroy(gameObject);
    }


    void OnCollisionEnter(Collision collisionInfo)
    {

        if ((collisionInfo.gameObject.tag == "Floor" | collisionInfo.gameObject.tag == "Rampe") && gameObject.tag == "Apple")
        {
            float currentX = gameObject.transform.position.x;
            float currentY = gameObject.transform.position.y + 0.05f;
            float currentZ = gameObject.transform.position.z;

            Destroy(gameObject);
            Instantiate(BadApple, new Vector3(currentX, currentY, currentZ), Quaternion.identity);

        }
    }
}
