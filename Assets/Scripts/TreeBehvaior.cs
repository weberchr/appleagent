﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeBehvaior : MonoBehaviour
{
    public bool respawn;
    public int maxApples = 30;
    public int currentApples = 30;
    public GameObject applePrefab;
    public float respawnTimer = 30;
    public Material greenMat;
    public Material green2Mat;
    public Material brownMat;


    // Start is called before the first frame update
    void Start()
    {
        currentApples = 30;
        setTreeStatus();

    }

    // Update is called once per frame
    void Update()
    {
        respawnTimer -= Time.deltaTime;
        int seconds = (int)respawnTimer % 60;

        if (seconds <= 0)
        {
            respawnTimer = 30;
            if (currentApples < maxApples)
            {
                currentApples += 1;
            }
            setTreeStatus();

        }
    }

    public void setTreeStatus()
    {
        if (currentApples >= maxApples / 2)
        {
            transform.gameObject.tag = "Tree";
            Renderer rend = gameObject.GetComponent<Renderer>();
            rend.material = greenMat;
        }
        if (currentApples == 0)
        {
            transform.gameObject.tag = "EmptyTree";
            Renderer rend = gameObject.GetComponent<Renderer>();
            rend.material = brownMat;
        }
        else if (currentApples < maxApples / 2)
        {
            transform.gameObject.tag = "Tree";
            Renderer rend = gameObject.GetComponent<Renderer>();
            rend.material = green2Mat;
        }
    }


    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.name == "CollectorAgent")
        {
            float currentX = gameObject.transform.position.x;
            float currentZ = gameObject.transform.position.z;
            float collectorspeed = collisionInfo.transform.GetComponent<Rigidbody>().velocity.magnitude;

            float tanhResult = (Mathf.Exp(2.0f * collectorspeed) - 1) / (Mathf.Exp(2.0f * collectorspeed) + 1);
            int range = Mathf.RoundToInt(tanhResult*6f);


            if (currentApples < range)
            {
                range = currentApples;
            }
            if (range > 0)
            {
                print(range);
                //GameObject applePrefab = GameObject.FindWithTag("Apple");
                for (int i = 0; i < range; i++)
                {
                    float X = Random.Range(currentX - 0.4f, currentX + 0.4f);
                    float Y = Random.Range(5, 6);
                    float Z = Random.Range(currentZ - 0.4f, currentZ + 0.4f);

                    Instantiate(applePrefab, new Vector3(X, Y, Z), Quaternion.identity);
                }
                currentApples -= range;
            }
            setTreeStatus();


        }
    }
}

