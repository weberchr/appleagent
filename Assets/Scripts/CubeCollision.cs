﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCollision : MonoBehaviour
{
   void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.name == "FruitTree")
        {
            Debug.Log("Collision with Tree");
        }

        if (collisionInfo.gameObject.name == "Apple")
        {
            Debug.Log("Collision with Apple");
        }
        
    }
}
