﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    public GameObject playerObject;
    public float rotationSpeed;

 
    void FixedUpdate()
    {
        transform.RotateAround(playerObject.transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
    }
}
